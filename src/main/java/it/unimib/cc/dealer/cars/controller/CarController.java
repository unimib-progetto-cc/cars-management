package it.unimib.cc.dealer.cars.controller;

import java.util.ArrayList;
import java.util.Arrays;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import it.unimib.cc.dealer.cars.model.Car;
import it.unimib.cc.dealer.cars.model.Cars;
import it.unimib.cc.dealer.cars.security.SecurityUtils;
import it.unimib.cc.dealer.cars.service.CarService;
import lombok.extern.slf4j.Slf4j;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/cars")
@Slf4j
public class CarController {

    @Autowired
    private CarService carService;

    @GetMapping(value = "",
            produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    public Cars getCars(HttpServletRequest request,
    		@RequestParam(required = false, defaultValue="") String model, 
    		@RequestParam(required = false, defaultValue="") String vin, 
    		@RequestParam(required = false, defaultValue="") String manufacturer,
    		@RequestParam(required = false, defaultValue="") String color, 
    		@RequestParam(required = false, defaultValue="") String licensePlate,
    		@RequestParam(required = false, defaultValue="") String description) {
    	SecurityUtils.checkParameters(request, Arrays.asList(new String[]{"model", "vin", "manufacturer", "color", "licensePlate", "description"}));
    	Cars cars = carService.findByFieldsContainingIgnoreCase(model, vin, manufacturer, color, licensePlate, description);
    	cars.getCars().forEach( (car) -> car.add(
    			WebMvcLinkBuilder.linkTo(CarController.class).slash(car.getId()).withSelfRel()));
    	cars.add(WebMvcLinkBuilder.linkTo(CarController.class).withSelfRel());
        log.info("All cars found: " + cars);
        return cars;
    }

    @PostMapping(value = "",
    		produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE},
    		consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    public ResponseEntity<Car> createCar(HttpServletRequest request, @Valid @RequestBody Car car) {
    	SecurityUtils.checkParameters(request, new ArrayList<>());
    	Car createdCar = carService.save(car);
  		car.add(WebMvcLinkBuilder.linkTo(CarController.class).slash(createdCar.getId()).withSelfRel());
    	log.info("Car saved: " + createdCar);
        return new ResponseEntity <Car> (createdCar, HttpStatus.CREATED);
    }

    @GetMapping(value = "/{carId}",  
    		produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    public Car getCar(HttpServletRequest request, @PathVariable long carId) {
    	SecurityUtils.checkParameters(request, new ArrayList<>());
    	Car car = carService.findById(carId);
  		car.add(WebMvcLinkBuilder.linkTo(CarController.class).slash(car.getId()).withSelfRel());
  		log.info("Car found: " + car);
  		return car;
    }

    @PutMapping(value = "/{carId}",
    		produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE},
    		consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    public ResponseEntity<?> updateCar(HttpServletRequest request, @PathVariable long carId, 
    		@Valid @RequestBody Car carRequest) {
    	SecurityUtils.checkParameters(request, new ArrayList<>());
    	carRequest.setId(carId);
    	Car updatedCar = carService.update(carRequest);
    	log.info("Car updated: " + updatedCar);
    	return ResponseEntity.noContent().build();
    }

    @DeleteMapping(value = "/{carId}",
    		produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    public Car deleteCar(HttpServletRequest request, @PathVariable long carId) {
    	SecurityUtils.checkParameters(request, new ArrayList<>());
    	Car deletedCar = carService.delete(carId);
    	log.info("Car deleted: " + deletedCar);
    	return deletedCar;
    }
}
