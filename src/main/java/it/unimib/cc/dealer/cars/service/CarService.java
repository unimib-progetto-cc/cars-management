package it.unimib.cc.dealer.cars.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.unimib.cc.dealer.cars.exception.ResourceNotFoundException;
import it.unimib.cc.dealer.cars.model.Car;
import it.unimib.cc.dealer.cars.model.Cars;
import it.unimib.cc.dealer.cars.repository.CarRepository;

@Service
public class CarService {

    @Autowired
    private CarRepository repository;

    public Car findById(long id) {
    	return repository.findById(id)
			.orElseThrow(() -> 
				new ResourceNotFoundException("Car not found with id " + id)
			);
    }
    
    public Car save(Car car) {
    	return repository.save(car);
    }
    
    public Car update(Car car) {
    	return repository.findById(car.getId())
    			.map(oldCar -> {
    				return repository.save(car);
    			})
    			.orElseThrow(() -> 
    				new ResourceNotFoundException("Car not found with id " + car.getId())
				);
    }
    
    public Car delete(long id) {
    	return repository.findById(id)
    			.map(car -> {
                	repository.delete(car);
                    return car;
                })
    			.orElseThrow(() -> 
    				new ResourceNotFoundException("Car not found with id " + id)
				);
    }
    
    public Cars findByFieldsContainingIgnoreCase(String model, String vin, String manufacturer, String color, String licensePlate, String description) {
    	return new Cars(repository.findByModelContainingIgnoreCaseAndVinContainingIgnoreCaseAndManufacturerContainingIgnoreCaseAndColorContainingIgnoreCaseAndLicensePlateContainingIgnoreCaseAndDescriptionContainingIgnoreCase(
    			model, vin, manufacturer, color, licensePlate, description));
    }
}
