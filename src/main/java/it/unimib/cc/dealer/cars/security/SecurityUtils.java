package it.unimib.cc.dealer.cars.security;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import it.unimib.cc.dealer.cars.exception.BadRequestException;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class SecurityUtils {
    
    public static final void checkParameters(HttpServletRequest request, List <String> allowedParameters) {
        ArrayList<String> actualParameters = Collections.list(request.getParameterNames());
        if (actualParameters.size() > allowedParameters.size()) {
            actualParameters.removeAll(allowedParameters);
            log.error("Parameters not allowed: " + actualParameters);
        	if (!actualParameters.isEmpty()) {
        		throw new BadRequestException("Parameters not allowed: " + actualParameters);
        	}
        }
    }
}
