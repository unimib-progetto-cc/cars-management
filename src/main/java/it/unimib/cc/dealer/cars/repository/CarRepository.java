package it.unimib.cc.dealer.cars.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import it.unimib.cc.dealer.cars.model.Car;

public interface CarRepository extends JpaRepository<Car, Long> {
	Optional<Car> findById(long carId);
	List<Car> findByModelContainingIgnoreCaseAndVinContainingIgnoreCaseAndManufacturerContainingIgnoreCaseAndColorContainingIgnoreCaseAndLicensePlateContainingIgnoreCaseAndDescriptionContainingIgnoreCase(String model, String vin, String manufacturer, String color, String licensePlate, String description);
}
