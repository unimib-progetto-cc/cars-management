package it.unimib.cc.dealer.cars;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CarsManagemenApplication {

	public static void main(String[] args) {
		SpringApplication.run(CarsManagemenApplication.class, args);
	}
}
