package it.unimib.cc.dealer.cars.model;

import java.util.ArrayList;
import java.util.List;

import org.springframework.hateoas.RepresentationModel;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

@JacksonXmlRootElement(localName = "cars")
public class Cars extends RepresentationModel<Cars> {
	
    @JacksonXmlProperty(localName = "car")
    @JacksonXmlElementWrapper(useWrapping = false)
    private List<Car> cars = new ArrayList<>();

	public Cars(List<Car> cars) {
		super();
		this.cars = cars;
	}

	public List<Car> getCars() {
		return cars;
	}

	public void setCars(List<Car> cars) {
		this.cars = cars;
	}

	@Override
	public String toString() {
		return "Cars [cars=" + cars + "]";
	}
}
