package it.unimib.cc.dealer.cars.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import org.springframework.hateoas.RepresentationModel;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import io.swagger.annotations.ApiModelProperty;

@Entity
@Table(name = "car")
@JacksonXmlRootElement(localName = "car")
public class Car extends RepresentationModel<Car> {

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="car_id")
	@ApiModelProperty(readOnly = true)
    private long id;

    @NotBlank
    @Size(min = 1, max = 100)
    @Column(nullable = false)
    private String model;

    @NotBlank
    @Size(min = 17, max = 17)
    @Column(nullable = false)
    private String vin;

    @NotBlank
    @Size(min = 1, max = 25)
    @Column(nullable = false)
    private String manufacturer;

    @NotBlank
    @Size(min = 1, max = 50)
    @Column(nullable = false)
    private String color;

    @NotBlank
    @Size(min = 7, max = 7)
    @Column(nullable = false)
    private String licensePlate;

    @NotBlank
    @Size(min = 1, max = 500)
    @Column(nullable = false)
    private String description;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getVin() {
		return vin;
	}

	public void setVin(String vin) {
		this.vin = vin;
	}

	public String getManufacturer() {
		return manufacturer;
	}

	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getLicensePlate() {
		return licensePlate;
	}

	public void setLicensePlate(String licensePlate) {
		this.licensePlate = licensePlate;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return "Car [id=" + id + ", model=" + model + ", vin=" + vin + ", manufacturer=" + manufacturer + ", color="
				+ color + ", licensePlate=" + licensePlate + ", description=" + description + "]";
	}
}
