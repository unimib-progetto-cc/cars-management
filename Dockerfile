# Build phase.
FROM maven:3.5-jdk-8 as build
COPY ./ /data
WORKDIR /data
RUN mvn clean package -Dmaven.test.skip=true

# Deploy phase.
FROM openjdk:8-jre-alpine
ARG VERSION
COPY --from=build /data/target/cars-$VERSION-SNAPSHOT.jar /app/cars-app.jar
EXPOSE 8080
WORKDIR /app
ENTRYPOINT ["java", "-jar", "cars-app.jar"]